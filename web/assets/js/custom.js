$(window).on('load', function () {});
$(document).ready(function () {

  $("#main-menu .navbar-nav li").hover(function () {
    $(this).addClass("hover").siblings().removeClass("hover")
  });

  $(function () {
    var current = location.pathname;
    $('#main-menu .navbar-nav li a').each(function () {
      var $this = $(this);
      // if the current path is like this link, make it active
      if ($this.attr('href').indexOf(current) !== -1) {
        $this.addClass('active');
      }
    })
  });

  var stickyTop = $('#main-menu').offset().top;
  $(window).on('scroll', function () {
    if ($(window).scrollTop() >= 200) {
      $('#main-menu').addClass('isScroll').removeClass('isTop');
    } else {
      $('#main-menu').addClass('isTop').removeClass('isScroll');
    }
  });

  $('#promo-banner').slick({

  });

  $('#testi-banner').slick({
    prevArrow: $('.prev'),
    nextArrow: $('.next')
  });
});
