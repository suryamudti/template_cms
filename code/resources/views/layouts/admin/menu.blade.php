<ul class="list">
    <li class="header">MAIN MENU</li>
    <li class="{{ Request::is('admin') ? 'active' : ''}}"><a href="{{ url('/admin') }}"><i class="fa fa-home"></i><span>Dashboard</span></a></li>

    @if(Auth::user()->role ==  \App\User::ROLE_SUPERADMIN)
        <li class="{{ (Request::is('admin/users*') || Request::is('admin/profile*'))? 'active' : ''}}"><a href="{{ url('/admin/users') }}"><i class="fa fa-group"></i><span>Users</span></a></li>
    @endif

    <li class="{{ Request::is('admin/profile*') ? 'active' : ''}}" style="display: none"><a href="{{ url('/admin/profile') }}"><i class="fa fa-user"></i><span>Profile</span></a></li>

    {{--<li>--}}
        {{--<a href="javascript:void(0);" class="menu-toggle">--}}
            {{--<i class="material-icons">widgets</i>--}}
            {{--<span>Widgets</span>--}}
        {{--</a>--}}
        {{--<ul class="ml-menu">--}}
            {{--<li>--}}
                {{--<a href="javascript:void(0);" class="menu-toggle">--}}
                    {{--<span>Cards</span>--}}
                {{--</a>--}}
                {{--<ul class="ml-menu">--}}
                    {{--<li>--}}
                        {{--<a href="pages/widgets/cards/basic.html">Basic</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="pages/widgets/cards/colored.html">Colored</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="pages/widgets/cards/no-header.html">No Header</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</li>--}}
</ul>
