<!--Styles-->
<link href="{{ url('/web/assets/vendor/font-awesome/css/fontawesome-all.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/web/assets/vendor/bootstrap/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('/web/assets/vendor/bootstrap/css/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css">

<link href="{{ url('/web/assets/vendor/slick-slider/slick.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('/web/assets/vendor/slick-slider/slick-theme.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('/web/assets/vendor/owl-carousel/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">

<!--Fonts-->

<!--Mechanic Styles-->
<link rel="stylesheet" href="{{ url('/') }}/web/assets/css/style.css">
<link rel="stylesheet" href="{{ url('/') }}/web/assets/css/fonts.css">
<link rel="stylesheet" href="{{ url('/') }}/web/assets/css/default.css">
<link rel="stylesheet" href="{{ url('/') }}/web/assets/css/responsive.css">


<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/web/vendors/rs-plugin/css/settings-ie8.css" media="screen">
<script src="{{ url('/') }}/web/js/html5shiv.min.js"></script>
<script src="{{ url('/') }}/web/js/respond.min.js"></script>
<![endif]-->
