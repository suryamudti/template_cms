<!-- Vendor JS -->
<script src="{{url('/')}}/web/assets/vendor/jquery/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/web/assets/vendor/jquery/js/jquery.js" type="text/javascript"></script>
<script src="{{url('/')}}/web/assets/vendor/jquery/js/jquery-ui.js" type="text/javascript"></script>
<script src="{{url('/')}}/web/assets/vendor/jquery/js/moment.js" type="text/javascript"></script>
<script src="{{url('/')}}/web/assets/vendor/bootstrap/js/tether.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/web/assets/vendor/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<script src="{{url('/')}}/web/assets/vendor/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/web/assets/vendor/owl-carousel/js/owl.carousel.js" type="text/javascript"></script>
<script src="{{url('/')}}/web/assets/vendor/slick-slider/slick.js" type="text/javascript"></script>

<!-- Custom JS -->
<script src="{{url('/')}}/web/assets/js/custom.js" type="text/javascript"></script>