php artisan crud:generate Test --fields="nama#string; harga#integer; unit#integer" --view-path=admin --controller-namespace=Admin --route-group=admin

php artisan crud:generate Song --fields="artist_alias#text; artist#text; song#text; song_alias#text; artist_song#text" --view-path=admin --controller-namespace=Admin --route-group=admin
